import React from 'react'
import { TimePicker } from '../TimePicker'

type Props = {
  timeslot: any
}

const ReservationTimePicker: React.FC<Props> = ({ timeslot }) => {
  return (
    <div className="ui card centered" style={{ height: '500px', overflowY: 'scroll'}}>
      <div className="content">
        <TimePicker groups={timeslot} />
      </div>
    </div>
  )
}


export default ReservationTimePicker;