import React from 'react'
import DateTimeLabel from '../../Other/DateTimeLabel/DateTimeLabel';
import HeaderTitle from '../../Other/HeaderTitle/HeaderTitle'

type Props = {
  groups: any;
}

export const TimePicker: React.FC<Props> = ({ groups }) => {
  if (!groups) {
    return null;
  }

  return (
    <>
      {
        groups.map((item: any) => (
          <React.Fragment key={item.id}>
            <HeaderTitle title={item.day} />
            {item.time_slots.map((time: any, index: any) => <DateTimeLabel key={time.id} day_time={time} /> )}
          </React.Fragment>
        ))
      }
    </>
  )
}