import React from 'react'
import { useReservationState } from '../../../hooks/company-reservation'
import { TimeslotProvider } from '../../../hooks/timeslot-context'
import HeaderTitle from '../../Other/HeaderTitle/HeaderTitle'
import LabelWithDescription from '../../Other/LabelWithDescription/LabelWithDescription'
import ReservationTimePicker from '../ReservationTimePicker/ReservationTimePicker'

type Props = {
  data: any;
}

const Reservation: React.FC<Props> = ({ data }) => {
  return (
    <div className="column" style={{ paddingTop: '10px' }}>
      <div className="ui segment">
        <TimeslotProvider>
          <HeaderTitle title={data.company} />
          <div className="ui divider"></div>
          <LabelWithDescription />
          <ReservationTimePicker timeslot={data.groups} />
        </TimeslotProvider>
      </div>
    </div>
  )
}

export default Reservation;