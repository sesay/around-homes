import React, { Component } from 'react'
import timeslotData from '../../../helpers/timeslotData';
import Reservation from '../Reservation/Reservation'

type Props = {
  data: any
}
class CompanyReservation extends Component<Props> {

  render() {
    const data = timeslotData(this.props.data);

    return (
      <div style={{ margin: '20px' }} className="ui three column grid">
        {
          data.map((item: any) => <Reservation data={item} key={item.id} />)
        }
    </div>
    )
  }
}
export { CompanyReservation };