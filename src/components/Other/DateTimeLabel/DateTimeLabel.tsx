import React from 'react'
import { useReservationDispatch, useReservationState } from '../../../hooks/company-reservation';
import { useTimeslotDispatch, useTimeslotState } from '../../../hooks/timeslot-context';

type Props = {
  day_time: any;
}

const DateTimeLabel: React.FC<Props> = ({ day_time }) => {
  const { start_time, end_time, dayOfTheWeek, id } = day_time;

  // Reservation State
  const dispatch = useTimeslotDispatch()
  const { timeslot } = useTimeslotState()

  // Reservation State
  const { reservation } = useReservationState()
  const reservationDispatch = useReservationDispatch();

  const isActive = day_time.id === timeslot.id;

  const handleBtnClick = () => {
    dispatch({ type: 'addtimeslot', payload: { start_time, end_time, dayOfTheWeek, id } })
    reservationDispatch({ type: 'addtimeslot', payload: { id } })
  }

  // const handleReset = () => {
  //   dispatch({ type: 'removetimeslot' })
  // }

  return (
    <div className="center aligned" style={{ margin: '10px 0' }}>
      <button
        className={
          `ui basic button ${isActive ? 'positive' : ''} 
          ${(reservation.id === day_time.id && !isActive) ? 'red disabled' : ''}`
        }
        onClick={handleBtnClick}
        data-button-id={day_time.id}
      >
        {start_time} - {end_time}
      </button>
      {/* { isActive && (<div onClick={() => handleReset()}>clear</div>) } */}
    </div>
  )
}

export default DateTimeLabel;