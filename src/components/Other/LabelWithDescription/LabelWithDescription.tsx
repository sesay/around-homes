import React from 'react'
import { useTimeslotState } from '../../../hooks/timeslot-context';

const LabelWithDescription = () => {

  const { timeslot } = useTimeslotState();

  if (!timeslot) {
    return null;
  }

  const { start_time, end_time, dayOfTheWeek } = timeslot;

  return (
    <div className="" style={{ textAlign: 'center'}}>
      { timeslot ? (
        <h2 className="ui header green label centered">
          reservation
          <div className="sub header">
            <span>{dayOfTheWeek.substring(0, 3)}</span>: {start_time} - {end_time}
          </div>
        </h2>
      ) : <p>Select a reservation time</p>}
    </div>
  )
}

export default LabelWithDescription;
