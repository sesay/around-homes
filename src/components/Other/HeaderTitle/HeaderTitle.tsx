import React from 'react'

type Props = {
  title: string;
}

const HeaderTitle: React.FC<Props> = ({ title }) => {
  return (
    <div>
      <h2 className="ui header center aligned">{title}</h2>
    </div>
    
  )
}
export default HeaderTitle;
