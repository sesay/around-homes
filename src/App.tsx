import React from 'react'
import { CompanyReservation } from './components/TimeSlot/CompanyReservation/CompanyReservation'
import { data } from './data/data';
import { ReservationProvider } from './hooks/company-reservation';

const App = () => {
  return (
      <ReservationProvider>
        <CompanyReservation data={data}/>
      </ReservationProvider>
    )
}

export default App;