export const sortDateTime = (data: any) => {
  return data.sort((a: any, z: any) => {
    return a.end_time - z.end_time;
  });
};
