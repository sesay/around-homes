import { formatGroups } from './formatGroups';
import { groupByDayOfWeek } from './groupByDay';
import { sortDateTime } from './sortDateTime';

type DataType = {
  id: number;
  name: string;
  type: string;
  [propName: string]: any;
}

const timeslotData = (data: DataType) => {
  return data.map((item: { id: number, name: string}, index: number) => {
    let obj = {} as any;

    obj["id"] = item.id;
    obj["company"] = item.name;

    const sortedDateTime = sortDateTime(data[index].time_slots);
    const groupedByDay = groupByDayOfWeek(sortedDateTime);
    const groups= formatGroups(groupedByDay);

    obj["groups"] = groups;

    return obj;
  });
};

export default timeslotData;