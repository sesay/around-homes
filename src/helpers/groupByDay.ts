const weekdays = [
  "Sunday",
  "Monday",
  "Tuesday",
  "Wednesday",
  "Thursday",
  "Friday",
  "Saturday",
];

export const groupByDayOfWeek = (data: any) => {
  return data.reduce((acc: any, item: any, index: any) => {
    const startTime = new Date(item.start_time);
    const padStartTime = startTime
      .getMinutes()
      .toString()
      .padEnd(2, "0");

    const endTime = new Date(item.end_time);
    const padEndTime = endTime.getMinutes().toString().padEnd(2, "0");
    const dayOfTheWeek = weekdays[endTime.getDay()];

    if (!acc[dayOfTheWeek]) {
      acc[dayOfTheWeek] = [];
    }

    acc[dayOfTheWeek].push({
      id: index,
      dayOfTheWeek: dayOfTheWeek,
      start_time: `${startTime.getHours()}:${padStartTime}`,
      end_time: `${endTime.getHours()}:${padEndTime}`,
    });

    return acc;
  }, {});
};
