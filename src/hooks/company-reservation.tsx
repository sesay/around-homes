import * as React from 'react'

type Action = {type: 'addtimeslot', payload: any}
type Dispatch = (action: Action) => void
type State = {reservation: any}
type ReservationProviderProps = {children: React.ReactNode}

const ReservationStateContext = React.createContext<State | undefined>(undefined)
const ReservationDispatchContext = React.createContext<Dispatch | undefined>(undefined)

function reservationReducer(state: State, action: Action) {
  switch (action.type) {
    case 'addtimeslot': {
      const result = action.payload;
      return {
        ...state,
        reservation: result
      };
    }
    default: {
      // @ts-ignore
      throw new Error(`Unhandled action type: ${action.type}`)
    }
  }
}

function ReservationProvider({children}: ReservationProviderProps) {
  const [state, dispatch] = React.useReducer(reservationReducer, {reservation: ''})
  return (
    <ReservationStateContext.Provider value={state}>
      <ReservationDispatchContext.Provider value={dispatch}>
        {children}
      </ReservationDispatchContext.Provider>
    </ReservationStateContext.Provider>
  )
}

function useReservationState() {
  const context = React.useContext(ReservationStateContext)
  if (context === undefined) {
    throw new Error('useReservationState must be used within a ReservationProvider')
  }
  return context
}

function useReservationDispatch() {
  const context = React.useContext(ReservationDispatchContext)
  if (context === undefined) {
    throw new Error('useReservationDispatch must be used within a ReservationProvider')
  }
  return context
}

export {ReservationProvider, useReservationState, useReservationDispatch}
