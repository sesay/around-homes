import * as React from 'react'

type Action = {type: 'addtimeslot', payload: any} | {type: 'removetimeslot', payload?: any}
type Dispatch = (action: Action) => void
type State = {timeslot: any}
type TimeslotProviderProps = {children: React.ReactNode}

const TimeslotStateContext = React.createContext<State | undefined>(undefined)
const TimeslotDispatchContext = React.createContext<Dispatch | undefined>(undefined)

function timeslotReducer(state: State, action: Action) {
  switch (action.type) {
    case 'addtimeslot': {
      const result = action.payload;
      return {
        ...state,
        timeslot: result
      };
    }
    case 'removetimeslot': {
      return {
        ...state,
        timeslot: null
      };
    }
    default: {
      // @ts-ignore
      throw new Error(`Unhandled action type: ${action.type}`)
    }
  }
}

function TimeslotProvider({children}: TimeslotProviderProps) {
  const [state, dispatch] = React.useReducer(timeslotReducer, {timeslot: ''})
  return (
    <TimeslotStateContext.Provider value={state}>
      <TimeslotDispatchContext.Provider value={dispatch}>
        {children}
      </TimeslotDispatchContext.Provider>
    </TimeslotStateContext.Provider>
  )
}

function useTimeslotState() {
  const context = React.useContext(TimeslotStateContext)
  if (context === undefined) {
    throw new Error('useTimeslotState must be used within a TimeslotProvider')
  }
  return context
}

function useTimeslotDispatch() {
  const context = React.useContext(TimeslotDispatchContext)
  if (context === undefined) {
    throw new Error('useTimeslotDispatch must be used within a TimeslotProvider')
  }
  return context
}

export {TimeslotProvider, useTimeslotState, useTimeslotDispatch}
