This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

`please use node-engine 12 and above :)`
### `yarn && yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.
### `learnings`

This is quite an interesting challenge handling the data was quite interesting, but i was able to work it out, took some time though.
- I tried re-use the components as much as possible, isolated each piece from the other
- 
### `helpers`
- i attached an image of my component structure to outline my thought process
- i used semantic ui for the component styling just for a quick prototype [https://semantic-ui.com/](https://semantic-ui.com/)
### `gotchas`
- making the timeslot sync across company, is doable but i didn't have enough time to think it through with a little bit of time, i would figure it out

### `TODO`
- Syncing the disabled timeslot across companies :) 